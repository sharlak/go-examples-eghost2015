package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func getPages() ([]string, error) {

	var result []string

	dir, err := os.Open("pages")
	if err != nil {
		return result, err
	}
	defer dir.Close()
	names, err := dir.Readdirnames(-1)
	if err != nil {
		return result, err
	}

	return names, err

}

func main() {
	http.HandleFunc("/api/list_pages/", func(w http.ResponseWriter, r *http.Request) {
		names, err := getPages()
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		data, err := json.Marshal(names)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		w.Write(data)
	})

	visitCount := make(map[string]int)

	http.HandleFunc("/pages/", func(w http.ResponseWriter, r *http.Request) {
		pagename := r.URL.Path
		visitCount[pagename] += 1
		// fmt.Fprintln(w, "Abriendo handler para la página: "+pagename)
		splitted := strings.Split(pagename, "/")
		filename := "pages/" + splitted[len(splitted)-1]
		data, err := ioutil.ReadFile(filename)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		fmt.Fprintln(w, "<html>	<body>")
		fmt.Fprintln(w, string(data))
		fmt.Fprintf(w, "<p>Visitas: %d</p>", visitCount[pagename])
		fmt.Fprintln(w, "</body></html>")
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		names, err := getPages()
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		fmt.Fprintln(w, "<html>	<body><h1>Blog del e-ghost</h1>")
		fmt.Fprintln(w, "<ul>")

		for i := 0; i < len(names); i++ {
			name := names[i]
			fmt.Fprintf(w, "<li><a href='/pages/%s'>%s</a></li>", name, name)
		}
		fmt.Fprintln(w, "</ul>")

		fmt.Fprintln(w, "</body></html>")
	})

	http.ListenAndServe(":8080", nil)
}
