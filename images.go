package main

import (
	"encoding/json"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"time"
)

type ImageInfo struct {
	FileName string `json:"filename"`
	Width    int    `json:"width"`
	Height   int    `json:"height"`
	Area     int    `json:"area"`
}

/*
func (i ImageInfo) Area() int {
	return i.Width * i.Height
}
*/
// Remember: to change the ImageInfo structure pass it by reference with *
func (i *ImageInfo) CalculateArea() {
	i.Area = i.Width * i.Height
}

// Error management function
func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

// Process a jpg image: scale it by 16 and save it in png
// Print image info (structure example)
func processImage(fileName string) {
	//  Open the file
	// file, err := os.Open("photos/" + fileName)
	file, err := os.Open(fileName)
	checkError(err)
	defer file.Close()

	// Decode from jpeg
	img, err := jpeg.Decode(file)
	checkError(err)

	// Scale initializations
	scale := 16
	outputWidth := img.Bounds().Dx() / scale
	outputHeight := img.Bounds().Dy() / scale

	// Initializating structures
	/*
		var info ImageInfo
		info.FileName = fileName
		info.Width = img.Bounds().Dx()
		info.Height = img.Bounds().Dy()
	*/
	info := ImageInfo{
		FileName: fileName,
		Width:    img.Bounds().Dx(),
		Height:   img.Bounds().Dy(),
	}

	// Structure method calling examples
	info.CalculateArea()
	// fmt.Println(info)
	// fmt.Println(info.Area())

	data, err := json.MarshalIndent(info, "", "")
	checkError(err)

	fmt.Println(string(data))

	newImage := image.NewRGBA(image.Rect(0, 0, outputWidth, outputHeight))

	for x := 0; x < outputWidth; x++ {
		for y := 0; y < outputHeight; y++ {
			newImage.Set(x, y, img.At(x*scale, y*scale))
		}
	}
	outputFileName := strings.Replace(fileName, ".JPG", ".png", 1)
	outFile, err := os.Create("output/" + outputFileName)
	checkError(err)
	defer outFile.Close()
	err = png.Encode(outFile, newImage)
	checkError(err)

}

func printMoo() {
	for {
		fmt.Println("moo")
		time.Sleep(500 * time.Millisecond)
	}
}

// Important: wg passed by reference
// If we pass it by value we would be updating different wait groups
func imageProcessor(filesChan chan string, wg *sync.WaitGroup) {
	for {
		// Note the syntax to extract from the channel
		fileName := <-filesChan
		fmt.Println("Received:" + fileName)
		processImage(fileName)
		// Inform the wg that the goroutine has ended
		wg.Done()
	}
}

func main() {
	numCores := runtime.NumCPU()
	// This line tells Go to use as many threads as numCores
	// By default it uses 1
	runtime.GOMAXPROCS(numCores)

	// Define a channel that is synchronized between threads
	// FIFO stack
	fileNamesChannel := make(chan string)

	// Define a waitgroup for sync
	var wg sync.WaitGroup

	for i := 0; i < numCores; i++ {
		// Important: wg passed by reference
		// Execute it in goroutines (threads)
		go imageProcessor(fileNamesChannel, &wg)
	}

	var names []string

	/*
		names := []string{
			"P1050178.JPG", "P1050209.JPG", "P1050583.JPG", "P1050600.JPG", "P1050642.JPG", "P1050654.JPG",
			"P1050179.JPG", "P1050376.JPG", "P1050584.JPG", "P1050601.JPG", "P1050646.JPG", "P1050657.JPG",
			"P1050181.JPG", "P1050390.JPG", "P1050585.JPG", "P1050602.JPG", "P1050647.JPG", "P1050659.JPG",
			"P1050183.JPG", "P1050441.JPG", "P1050592.JPG", "P1050612.JPG", "P1050648.JPG", "P1050667.JPG",
			"P1050201.JPG", "P1050456.JPG", "P1050594.JPG", "P1050616.JPG", "P1050649.JPG", "P1050673.JPG",
			"P1050204.JPG", "P1050460.JPG", "P1050597.JPG", "P1050623.JPG", "P1050650.JPG", "P1050674.JPG",
		}
	*/

	// Print all the content of a directory
	filepath.Walk("photos", func(path string, info os.FileInfo, err error) error {
		fmt.Println(path)
		if strings.HasSuffix(path, ".JPG") {
			names = append(names, path)
		}
		return nil
	})

	// This line adds the number of images to process tp the wait group, that
	// means that you have to wait for len(names) goroutines to end
	wg.Add(len(names))
	/*
		go printMoo()

		for _, fileName := range names {
			fmt.Println("Procesando archivo: " + fileName)
			processImage(fileName)

		}

	*/

	// Load the channel with the filenames
	for _, fileName := range names {
		// Note the syntax to add an item to a channel
		fileNamesChannel <- fileName
	}

	// Wait for the gorutines to end
	wg.Wait()
	fmt.Println("Done!")
}
