# README #

This repository contains examples for the course Introduction to Go, by [Jon Valdés](http://hombrealto.com/) ([in Github](https://github.com/jonvaldes)), given during 2015 eGhost Summer Courses.

## Testing the code ##

If you have Go installed, you can test any of the examples by running:

```
#!bash

$ go run [example-name].go
```

## Course videos ##
You can view the course videos in Youtube (in Spanish):

* [Part 0: Introduction and methodology](https://www.youtube.com/watch?v=1yZR9GrVyjE)
* [Part 1: Day 1, first examples](https://www.youtube.com/watch?v=xrstW45CceE)
* [Part 2: Day 2, blog example](https://www.youtube.com/watch?v=VI4xxATxlNw)

## eGhost ##
You can also [visit the eGhost page](http://www.e-ghost.deusto.es/) or watch more courses in [eGhost Youtube channel](https://www.youtube.com/channel/UCO2x6s8_efwbAOTHZ_zdqqg).
